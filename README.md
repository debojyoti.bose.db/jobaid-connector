# jobaid-connector project frontend and backend local setup

If you new to VueJs, see the steps to create sample vue project - https://www.tutorialspoint.com/vuejs/vuejs_environment_setup.htm

# SSH Key generation 

step1. ssh-keygen -t ed25519 -C "<comment>"

step2. Paste the following in the ssh key 
cat ~/.ssh/id_ed25519.pub | clip

step3. test the connection
git clone git@gitlab.com:gitlab-tests/sample-project.git

 
=======================================================================
# Docker repo

https://hub.docker.com/repository/docker/debojyotibosedb/jobaid


# GCP ssetup and create docker image

ssh-keygen -t ed25519 -C "keygen"

gcloud compute os-login ssh-keys add \
    --key-file=/home/student_03_039b665b07b4/.ssh/id_ed25519.pub \

git clone https://gitlab.com/debojyoti.bose.db/jobaid-connector.git


cd jobaid-connector/jobaid-frontend
docker build . -t jobaid-frontend:v.01


cd jobaid-connector/jobaid-backend
docker build . -t jobaid-backend:v.01


cd jobaid-connector/cms
docker build . -t jobaid-cms:v.01


# docker login and push images to docker hub

docker images


docker tag <image_tag> debojyotibosedb/jobaid:jobaid-frontend-v.01
e.g. docker tag ca3712945eaf debojyotibosedb/jobaid:jobaid-frontend-v.01

docker push debojyotibosedb/jobaid:jobaid-frontend-v.01



docker tag <image_tag> debojyotibosedb/jobaid:jobaid-backend-v.01
e.g. docker tag c8e2b67bfdc2 debojyotibosedb/jobaid:jobaid-backend-v.01

docker push debojyotibosedb/jobaid:jobaid-backend-v.01


=======================================================================

# To delete all containers including its volumes use,

docker rm -vf $(docker ps -aq)

# To delete all the images,

docker rmi -f $(docker images -aq)

=======================================================================

install gradle - https://gradle.org/install/

node version 
node --version
v10.14.1

npm version
npm --version
6.4.1

=======================================================================

git clone git@gitlab.com:debojyoti.bose.db/jobaid-connector.git

=======================================================================

npm install vue

npm install vue-cli

npm install vue-router

npm install axios

npm i @vue/cli-service

npm i vue-template-compiler

# Run front end application 
To run the frontend application - cd ~/workspace/jobaid-connector/jobaid-frontend

npm run serve

=======================================================================

# MYsql setup

Install MySql and create a schema jobaid

spring.datasource.url=jdbc:mysql://localhost:3306/jobaid
spring.datasource.username=root
spring.datasource.password=system

execute the following query - 

CREATE TABLE `customer` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `first_name` VARCHAR(100) NULL,
  `last_name` VARCHAR(100) NULL,
  `email` VARCHAR(100) NULL,
  `phone` VARCHAR(20) NULL,
  PRIMARY KEY (`id`));
  
=======================================================================
